function goToIntroPage(){
    location.replace("resume_intro.html");
}
function goToAboutPage(){
    location.replace("resume_about.html");
}
function goToEducationPage(){
    location.replace("resume_education.html");
}
function goToProjectPage(){
    location.replace("resume_project.html");
}
function goToPortofolioPage(){
    location.replace("resume_portofolio.html");
}
function goToOtherPage(){
    location.replace("resume_other.html");
}

function showAnnotationText(){
    document.getElementById("scroll-down-text").style.opacity = "1";
}
function hideAnnotationText(){
    document.getElementById("scroll-down-text").style.opacity = "0";
}

/* HIGHLIGHT MENU */

if((window.location.href).match(/resume_about\.html/)){
    document.getElementById("menu-about-rec").style.background = "#000000";
    document.getElementById("menu-about-caption").style.color = "#ffffff";
    document.getElementById("menu-about-circle").style.background = "#000000";
}

if((window.location.href).match(/resume_education\.html/)){
    document.getElementById("menu-education-rec").style.background = "#000000";
    document.getElementById("menu-education-caption").style.color = "#ffffff";
    document.getElementById("menu-education-circle").style.background = "#000000";
}

if((window.location.href).match(/resume_project\.html/)){
    document.getElementById("menu-project-rec").style.background = "#000000";
    document.getElementById("menu-project-caption").style.color = "#ffffff";
    document.getElementById("menu-project-circle").style.background = "#000000";
}

if((window.location.href).match(/resume_other\.html/)){
    document.getElementById("menu-other-rec").style.background = "#000000";
    document.getElementById("menu-other-caption").style.color = "#ffffff";
    document.getElementById("menu-other-circle").style.background = "#000000";
}

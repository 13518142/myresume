# Web CV
> Made by Dwiani Yulia Ariyanti

This web is made to fulfill task from SYNAPSIS.ID to apply Front-End Developer Intern.

It contains general information about me, my education profile, project that i involved in, also additional task that have been given by SYNAPSIS.ID.

To run this web, you can go to folder named `src`, then run `resume_intro.html` in any browser. It's better to not open it at  Mozilla FireFox because the fonts are not loaded properly there.

### File Explanation
| Folder | Sub-Folder |Explanation |
| ------ | ------ | ------ |
| src | css | Contain all CSS files used |
|  | js | Contain all JavaScript files used |
|  | jquery |  Contain jQuery lib needed| 
| data | font | Contain all fonts used for this web |
|  | img | Contain all images used for this web |

This web consists of 5 pages `html` in total, but you can access all these pages trough any page you opened.

### Web Display
![intro](/data/img/intro.jpeg)
![about-me](/data/img/about-me.jpeg)
![skill](/data/img/skill.jpeg)
![interest](/data/img/interest.jpeg)
![school](/data/img/school.jpeg)
![organization](/data/img/organization.jpeg)
![portofolio](/data/img/portofolio.jpeg)
![task](/data/img/task.jpeg)
![mini-windows](/data/img/mini-windows.jpeg)